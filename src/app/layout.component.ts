import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent {
  title = 'testgoodbook';
  constructor(private router:Router){}
  student(type){
    if(type=='list'){
      this.router.navigate(['/student']);
    }
    if(type=='details'){
      this.router.navigate(['/studentdetails']);
    }
 
  }
}
